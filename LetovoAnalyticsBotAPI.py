# !/usr/bin/python3.9
# Created by arsikurin
#
# import os
# import sys
import requests as rq
import logging as log
from ics import Calendar
from telebot import types
from imaplib import IMAP4_SSL
from colourlib import Fg, Style
from email import message_from_string

DEBUG = True

final_url = "https://student.letovo.ru/index.php?r=student&student="
login_url = "https://student.letovo.ru/preferences_login.php"
calendar_url = "https://student.letovo.ru/index.php?ics"
if DEBUG:
    log.basicConfig(format=f"{Fg.Green}{Style.Bold}%(asctime)s{Fg.Reset} %(message)s{Style.Reset}",
                    level=log.DEBUG, handlers=[log.FileHandler("bot.log"), log.StreamHandler()])
else:
    log.basicConfig(format=f"{Fg.Green}{Style.Bold}%(asctime)s{Fg.Reset} %(message)s{Style.Reset}",
                    handlers=[log.FileHandler("bot.log"), log.StreamHandler()])


def init_user(chat_id):
    """
    initialize new user

    :param chat_id: id of the user
    """
    from LetovoAnalyticsBot import conn, c
    with conn:
        c.execute(
            "INSERT INTO users VALUES (:mail_address, :mail_password, :analytics_password, :analytics_login, :chat_id)",
            {"mail_address": None, "mail_password": None, "analytics_password": None, "analytics_login": None,
             "chat_id": chat_id})


def update_mail_address(mail_address, chat_id):
    """
    update student's mail address

    :param mail_address: mail address
    :param chat_id: id of the user
    """
    from LetovoAnalyticsBot import conn, c
    with conn:
        c.execute("UPDATE users SET mail_address = :mail_address WHERE chat_id = :chat_id",
                  {"mail_address": mail_address, "chat_id": chat_id})


def update_mail_password(mail_password, chat_id):
    """
    update student's mail password

    :param mail_password: mail password
    :param chat_id: id of the user
    """
    from LetovoAnalyticsBot import conn, c
    with conn:
        c.execute("UPDATE users SET mail_password = :mail_password WHERE chat_id = :chat_id",
                  {"mail_password": mail_password, "chat_id": chat_id})


def update_analytics_password(analytics_password, chat_id):
    """
    update student.letovo.ru password

    :param analytics_password: letovo analytics password
    :param chat_id: id of the user
    """
    from LetovoAnalyticsBot import conn, c
    with conn:
        c.execute("UPDATE users SET analytics_password = :analytics_password WHERE chat_id = :chat_id",
                  {"analytics_password": analytics_password, "chat_id": chat_id})


def update_analytics_login(analytics_login: str, chat_id: str) -> None:
    """
    update student.letovo.ru login

    :param analytics_login: letovo analytics login
    :param chat_id: id of the user
    """
    from LetovoAnalyticsBot import conn, c
    with conn:
        c.execute("UPDATE users SET analytics_login = :analytics_login WHERE chat_id = :chat_id",
                  {"analytics_login": analytics_login, "chat_id": chat_id})


def get_user(chat_id: int) -> tuple:
    """
    get user data

    :param chat_id: id of the user
    :return: user data
    """
    from LetovoAnalyticsBot import conn, c
    with conn:
        c.execute("SELECT * FROM users WHERE chat_id=:chat_id", {"chat_id": chat_id})
        return c.fetchone()


def generate_inline_keyboard(**kwargs: str) -> types.InlineKeyboardMarkup:
    """
    generate inline keyboard

    :param kwargs: value of the button, name of the button
    :return: inline keyboard markup
    """
    keyboard = types.InlineKeyboardMarkup()
    for name, value in kwargs.items():
        callback_button = types.InlineKeyboardButton(text=value, callback_data=name)
        keyboard.add(callback_button)
    return keyboard


def generate_reply_keyboard(**kwargs: int) -> types.ReplyKeyboardMarkup:
    """
    generate reply keyboard

    :param kwargs: name of the button, row of the button
    :return: reply keyboard markup
    """
    markup = types.ReplyKeyboardMarkup()
    reply_buttons = []
    tmp = -1

    for name, row in sorted(kwargs.items(), key=lambda item: int(item[1])):
        if row != tmp:
            reply_buttons.append([])
        reply_button = types.KeyboardButton("/" + name)
        reply_buttons[row].append(reply_button)
        tmp = row

    for row in reply_buttons:
        markup.row(*row)
    return markup


def is_empty(m) -> bool:
    """
    check for emptiness

    :param m: any list
    :return: is it empty
    """
    return len(m) == 0


def get_otp(call) -> str:
    """
    get OTP from student's mail

    :param call: call id
    :return: One Time Password
    """

    mail = IMAP4_SSL("outlook.office365.com")
    mail.login(get_user(call.message.chat.id)[0], get_user(call.message.chat.id)[1])
    mail.list()
    mail.select("inbox")
    res, data = mail.search(None, "ALL")
    res, data = mail.fetch(data[0].split()[-1], "(RFC822)")
    otp = message_from_string(data[0][1].decode("utf-8"))
    otp = otp.get_payload().split("<b>")[2].split("=")[0].split("<")[0]
    return otp


def authorize_to_analytics(s: rq.Session, call) -> None:
    """
    authorize to student.letovo.ru

    :param s: Session of requests
    :param call: call id
    """
    # POST method N0
    login_data0 = {
        "act": "logg",
        "key": 1,
        "login": get_user(call.message.chat.id)[3],
        "pass": get_user(call.message.chat.id)[2]
    }
    r0 = s.post(login_url, login_data0)
    student = r0.content.decode().split('"')[3]

    # POST method N1
    login_data1 = {
        "act": "check_otp",
        "otp": 000000
    }
    r1 = s.post(final_url + student, login_data1)

    # POST method N2
    login_data2 = {
        "act": "check_otp",
        "otp": get_otp(call)
    }
    r2 = s.post(final_url + student, login_data2)


def get_calendar(call) -> list[list]:
    """
    parse calendar from student.letovo.ru

    :param call: call id
    :return: nested lists of lessons
    """
    info = []
    with rq.Session() as s:
        authorize_to_analytics(s, call)

        # Parse calendar
        ind = 0
        day = -1
        for ch, e in enumerate(list(Calendar(s.get(calendar_url).text).timeline)):
            try:
                desc = e.description.split()
            except IndexError:
                desc = []

            if ind != 0 and date != (date := ".".join(reversed(str(e.begin).split("T")[0].split("-")))):
                info.append([])
                day += 1
                ind = 0
            elif ind == 0:
                date = ".".join(reversed(str(e.begin).split("T")[0].split("-")))
                info.append([])
                day += 1

            info[day].append([])
            if (name := e.name.split()[0]) == "ММА":
                info[day][ind].append("*ММА*")
            elif name == "Плавание,":
                info[day][ind].append("*Плавание*")
            elif name == "Ассамблея,":
                info[day][ind].append("*Ассамблея*")
            else:
                info[day][ind].append(f"*{e.name}*")
            if not is_empty(desc):
                info[day][ind].append(f"[ZOOM]({desc[-1]})")
            info[day][ind].append(f'*{e.begin.strftime("%A")}*')
            info[day][ind].append(f'*{str(e.begin).split("T")[1].rsplit(":", 2)[0]}*')
            info[day][ind].append(f'*{str(e.end).split("T")[1].rsplit(":", 2)[0]}*')
            info[day][ind].append(f"*{e.location}*")
            info[day][ind].append(f'*{date}*')
            ind += 1
        return info


def send_certain_day_calendar(certain_day: int, call) -> None:
    """
    send certain day from calendar

    :param certain_day: day number (0-6)
    """
    from LetovoAnalyticsBot import bot

    markup = generate_reply_keyboard(start=0)
    bot.answer_callback_query(callback_query_id=call.id, show_alert=True, text="Wait 6 secs")
    bot.delete_message(call.message.chat.id, call.message.message_id)

    if int(certain_day) == -1:
        bot.send_message(call.message.chat.id, "_Congrats! It's Sunday, no lessons_", parse_mode="Markdown",
                         reply_markup=markup)
        keyboard = generate_inline_keyboard(authorization="Edit authorization data »",
                                            currentDayCalendar="Current day calendar",
                                            entireCalendar="Entire calendar",
                                            certainDayCalendar="Certain day calendar »")
        bot.send_message(call.message.chat.id, "Choose an option below ↴", reply_markup=keyboard)
    else:
        try:
            for ind, day in enumerate(get_calendar(call)):
                if ind == certain_day or certain_day == -10:
                    for lesson in day:
                        bot.send_message(call.message.chat.id, "\n".join(lesson), parse_mode="Markdown",
                                         reply_markup=markup, disable_notification=True)
        except Exception as err:
            print(Fg.Red, err, Fg.Reset)
            bot.send_message(call.message.chat.id, "_✗ Something went wrong! Try again in 3 secs ✗_",
                             parse_mode="Markdown", reply_markup=markup)
            bot.answer_callback_query(callback_query_id=call.id, show_alert=True,
                                      text="✗ Something went wrong! Try again in 3 secs ✗")
            keyboard = generate_inline_keyboard(authorization="Edit authorization data »",
                                                currentDayCalendar="Current day calendar",
                                                entireCalendar="Entire calendar",
                                                certainDayCalendar="Certain day calendar »")
            bot.send_message(call.message.chat.id, "Choose an option below ↴", reply_markup=keyboard)
