# !/usr/bin/python3.9
# Created by arsikurin
#
import sqlite3
from datetime import date
from telebot import TeleBot
from LetovoAnalyticsBotAPI import *

bot = TeleBot("1638159959:AAGTSWJV3FGcZLI98WWhKQuIKI1J4NGN_1s", threaded=False)
if DEBUG:
    log.basicConfig(format=f"{Fg.Green}{Style.Bold}%(asctime)s{Fg.Reset} %(message)s{Style.Reset}",
                    filename="bot.log", filemode="a", level=log.DEBUG)
else:
    log.basicConfig(format=f"{Fg.Green}{Style.Bold}%(asctime)s{Fg.Reset} %(message)s{Style.Reset}",
                    filename="bot.log", filemode="a")

with sqlite3.connect("users.sqlite") as conn:
    c = conn.cursor()


    @bot.message_handler(commands=["start"])
    def handle_start(message):
        """init bot"""
        markup = generate_reply_keyboard(start=0)

        if not get_user(chat_id := message.chat.id):
            init_user(chat_id)
            msg = bot.send_message(chat_id, "Send me your mail address", reply_markup=markup)
            bot.delete_message(message.chat.id, message.message_id)
            bot.register_next_step_handler(msg, add_mail_address)
        else:
            bot.delete_message(message.chat.id, message.message_id)
            keyboard = generate_inline_keyboard(authorization="Edit authorization data »",
                                                currentDayCalendar="Current day calendar",
                                                entireCalendar="Entire calendar",
                                                certainDayCalendar="Certain day calendar »")
            bot.send_message(message.chat.id, "Choose an option below ↴", reply_markup=keyboard)


    def add_mail_address(message, *msg2, ch=False):
        markup = generate_reply_keyboard(start=0)
        try:
            update_mail_address(message.text, message.chat.id)
            update_analytics_login(message.text.split("@")[0], message.chat.id)
            bot.delete_message(message.chat.id, message.message_id)
            if not ch:
                msg = bot.send_message(message.chat.id, "Send me your password for analytics", reply_markup=markup)
                if not is_empty(msg2):
                    bot.delete_message(message.chat.id, msg2[0].message_id)
                bot.register_next_step_handler(msg, add_analytics_password)
            else:
                if not is_empty(msg2):
                    bot.delete_message(message.chat.id, msg2[0].message_id)

                keyboard = generate_inline_keyboard(mail_address="Edit mail address",
                                                    analytics_password="Edit analytics password",
                                                    mail_password="Edit mail password",
                                                    back="« back")
                bot.edit_message_text(chat_id=msg2[1].message.chat.id, message_id=msg2[1].message.message_id,
                                      text="Choose an option below ↴", reply_markup=keyboard)
        except Exception as err:
            print(Fg.Red, err, Fg.Reset)


    def add_analytics_password(message, *msg2, ch=False):
        markup = generate_reply_keyboard(start=0)
        try:
            update_analytics_password(message.text, message.chat.id)
            bot.delete_message(message.chat.id, message.message_id)
            if not ch:
                if not is_empty(msg2):
                    bot.delete_message(message.chat.id, msg2[0].message_id)
                msg = bot.send_message(message.chat.id, "Send me your password for mail address", reply_markup=markup)
                bot.register_next_step_handler(msg, add_mail_password)
            else:
                if not is_empty(msg2):
                    bot.delete_message(message.chat.id, msg2[0].message_id)

                keyboard = generate_inline_keyboard(mail_address="Edit mail address",
                                                    analytics_password="Edit analytics password",
                                                    mail_password="Edit mail password",
                                                    back="« back")
                bot.edit_message_text(chat_id=msg2[1].message.chat.id, message_id=msg2[1].message.message_id,
                                      text="Choose an option below ↴", reply_markup=keyboard)
        except Exception as err:
            print(Fg.Red, err, Fg.Reset)


    def add_mail_password(message, *msg2):
        try:
            update_mail_password(message.text, message.chat.id)
            bot.delete_message(message.chat.id, message.message_id)
            if not is_empty(msg2):
                bot.delete_message(message.chat.id, msg2[0].message_id)

            keyboard = generate_inline_keyboard(mail_address="Edit mail address",
                                                analytics_password="Edit analytics password",
                                                mail_password="Edit mail password",
                                                back="« back")
            bot.edit_message_text(chat_id=msg2[1].message.chat.id, message_id=msg2[1].message.message_id,
                                  text="Choose an option below ↴", reply_markup=keyboard)
        except Exception as err:
            print(Fg.Red, err, Fg.Reset)


    @bot.message_handler(content_types=["text"])
    def any_msg(message):
        """clear garbage"""
        bot.delete_message(message.chat.id, message.message_id)


    @bot.callback_query_handler(func=lambda call: True)
    def callback_inline(call):
        """handle callbacks"""
        if call.message:
            if call.data == "certainDayCalendar":
                keyboard = generate_inline_keyboard(monday="Monday", tuesday="Tuesday", wednesday="Wednesday",
                                                    thursday="Thursday",
                                                    friday="Friday", saturday="Saturday", back="« back")
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Choose a day below ↴", reply_markup=keyboard)

            elif call.data == "currentDayCalendar":
                send_certain_day_calendar(int(date.today().strftime("%w")) - 1, call)

            elif call.data == "entireCalendar":
                send_certain_day_calendar(-10, call)

            elif call.data == "monday":
                send_certain_day_calendar(0, call)

            elif call.data == "tuesday":
                send_certain_day_calendar(1, call)

            elif call.data == "wednesday":
                send_certain_day_calendar(2, call)

            elif call.data == "thursday":
                send_certain_day_calendar(3, call)

            elif call.data == "friday":
                send_certain_day_calendar(4, call)

            elif call.data == "saturday":
                send_certain_day_calendar(5, call)

            elif call.data == "back":
                keyboard = generate_inline_keyboard(authorization="Edit authorization data »",
                                                    currentDayCalendar="Current day calendar",
                                                    entireCalendar="Entire calendar",
                                                    certainDayCalendar="Certain day calendar »")
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Choose an option below ↴", reply_markup=keyboard)

            elif call.data == "authorization":
                keyboard = generate_inline_keyboard(mail_address="Edit mail address",
                                                    analytics_password="Edit analytics password",
                                                    mail_password="Edit mail password",
                                                    back="« back")
                bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                      text="Choose an option below ↴", reply_markup=keyboard)

            elif call.data == "mail_address":
                msg = bot.send_message(call.message.chat.id, "OK. Send me your mail")
                bot.register_next_step_handler(msg, add_mail_address, msg, call, ch=True)

            elif call.data == "analytics_password":
                msg = bot.send_message(call.message.chat.id, "OK. Send me your password for analytics")
                bot.register_next_step_handler(msg, add_analytics_password, msg, call, ch=True)

            elif call.data == "mail_password":
                msg = bot.send_message(call.message.chat.id, "OK. Send me your password for mail address")
                bot.register_next_step_handler(msg, add_mail_password, msg, call)


    if __name__ == "__main__":
        bot.polling(none_stop=True)
